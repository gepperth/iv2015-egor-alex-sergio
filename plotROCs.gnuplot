set term pdf color linewidth 4
set key top center font 'Arial,15' spacing 4
set xlabel "True positive rate" font "Arial,16" offset 0,-0.3
set ylabel "False positive rate" font "Arial,16" offset -1
set xtics font "Arial,10"
set ytics font "Arial,10"
set key font "Arial,10" height 1
#set size 1,0.8
set bmargin 4.5
#set offsets graph 0,0,0,1





# 3 fusion strategies
set output "figs/rocsFusion.pdf"
plot [0:1] [0:1]  "results.txt" u ($4/($4+$3)):($2/($2+$5)) w lp  lw 4 ps 1 t "vision-->LIDAR",\
"results.txt" u ($8/($8+$7)):($6/($6+$9))  w lp lw 4 ps 1  t "LIDAR-->vision",\
"fnfp2distr.txt" u ($12/($12+$11)):($10/($10+$13)) w lp  lw 2 ps 1 t "complex",\
 "fnfp2mult.txt" u ($12/($12+$11)):($10/($10+$13))  w lp lw 2 ps 1  t "simple-mul",\
"fnfp2plus.txt" u ($12/($12+$11)):($10/($10+$13)) w lp  lw 2 ps 1 t "simple-add"


# standard case no fusion
set output "figs/rocs.pdf"
plot [0:1] [0:1]  "results.txt" u ($4/($4+$3)):($2/($2+$5)) w lp  lw 4 ps 1 t "vision-->LIDAR", "results.txt" u ($8/($8+$7)):($6/($6+$9))  w lp lw 4 ps 1  t "LIDAR-->vision"
# two additional size inputs no fusion
set output "figs/rocs2.pdf"
plot [0:1] [0:1]  "results2.txt" u ($4/($4+$3)):($2/($2+$5)) w lp  lw 4 ps 1 t "vision-->LIDAR", "results.txt" u ($8/($8+$7)):($6/($6+$9))  w lp lw 4 ps 1  t "LIDAR-->vision"
#pause -1

